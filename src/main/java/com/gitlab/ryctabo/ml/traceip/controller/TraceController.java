/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.controller;

import com.gitlab.ryctabo.ml.traceip.constraint.IpAddress;
import com.gitlab.ryctabo.ml.traceip.converter.IpTraceConverter;
import com.gitlab.ryctabo.ml.traceip.converter.IpTraceStringConverter;
import com.gitlab.ryctabo.ml.traceip.converter.StatisticConverter;
import com.gitlab.ryctabo.ml.traceip.converter.StatisticStringConverter;
import com.gitlab.ryctabo.ml.traceip.domain.dto.DistanceStatisticResponse;
import com.gitlab.ryctabo.ml.traceip.domain.dto.IpTraceResponse;
import com.gitlab.ryctabo.ml.traceip.service.DistanceStatisticService;
import com.gitlab.ryctabo.ml.traceip.service.IpTracerService;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@Validated
@RestController
@RequestMapping("/trace")
@RequiredArgsConstructor
public class TraceController {

    private final IpTracerService ipTracerService;

    private final DistanceStatisticService distanceStatisticService;

    private final IpTraceConverter ipTraceConverter;

    private final IpTraceStringConverter ipTraceStringConverter;

    private final StatisticConverter statisticConverter;

    private final StatisticStringConverter statisticStringConverter;

    @GetMapping("/statistics")
    public DistanceStatisticResponse getStatistics() {
        return Optional.of(distanceStatisticService.getStatistic())
                .map(statisticConverter::convert)
                .orElseThrow(this::throwsInternalServerError);
    }

    @GetMapping(value = "/statistics", produces = "text/plain")
    public String getStatisticsString() {
        return Optional.of(distanceStatisticService.getStatistic())
                .map(statisticConverter::convert)
                .map(statisticStringConverter::convert)
                .orElseThrow(this::throwsInternalServerError);
    }

    @GetMapping("/{ipAddress}")
    public IpTraceResponse get(@PathVariable @IpAddress String ipAddress) {
        return ipTracerService
                .trace(ipAddress)
                .map(ipTraceConverter::convert)
                .orElseThrow(this::throwsInternalServerError);
    }

    @GetMapping(value = "/{ipAddress}", produces = "text/plain")
    public String getString(@PathVariable @IpAddress String ipAddress) {
        return ipTracerService
                .trace(ipAddress)
                .map(ipTraceConverter::convert)
                .map(ipTraceStringConverter::convert)
                .orElseThrow(this::throwsInternalServerError);
    }

    private ResponseStatusException throwsInternalServerError() {
        return new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public Map<String, String> handleError(ConstraintViolationException exception) {
        HashMap<String, String> errors = new HashMap<>();
        exception
                .getConstraintViolations()
                .forEach(cv -> errors.put(cv.getPropertyPath().toString(), cv.getMessage()));
        return errors;
    }
}
