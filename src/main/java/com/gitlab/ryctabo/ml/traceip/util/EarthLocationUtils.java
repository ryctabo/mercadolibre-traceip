/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.util;

import com.gitlab.ryctabo.ml.traceip.client.domain.LocationData;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
public class EarthLocationUtils {

    /** Earth's mean radius in meter (6,371 Km) */
    public static final long EARTH_RADIUS = 6371L;

    private EarthLocationUtils() {
        throw new UnsupportedOperationException("the LocationUtils class can't be instantiated!");
    }

    private static BigDecimal rad(BigDecimal distance) {
        return BigDecimal.valueOf(Math.PI / 180).multiply(distance);
    }

    public static BigDecimal calculateDistance(LocationData from, LocationData to) {
        BigDecimal diffLat = rad(to.getLatitude().subtract(from.getLatitude()));
        BigDecimal diffLng = rad(to.getLongitude().subtract(from.getLongitude()));

        var a =
                Math.pow(Math.sin(diffLat.doubleValue() / 2d), 2)
                        + Math.cos(rad(from.getLatitude()).doubleValue())
                                * Math.cos(rad(to.getLatitude()).doubleValue())
                                * Math.pow(Math.sin(diffLng.doubleValue() / 2d), 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return BigDecimal.valueOf(EARTH_RADIUS * c).setScale(2, RoundingMode.UP);
    }
}
