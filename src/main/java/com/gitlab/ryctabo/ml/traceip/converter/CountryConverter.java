/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.converter;

import com.gitlab.ryctabo.ml.traceip.client.domain.CountryData;
import com.gitlab.ryctabo.ml.traceip.client.domain.CurrencyData;
import com.gitlab.ryctabo.ml.traceip.domain.dto.*;
import com.gitlab.ryctabo.ml.traceip.factory.CurrencyResponseFactory;
import com.gitlab.ryctabo.ml.traceip.factory.LocalTimeResponseFactory;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@Component
@RequiredArgsConstructor
public class CountryConverter implements Converter<CountryData, CountryResponse> {

    private final LanguageConverter languageConverter;

    private final CurrencyResponseFactory currencyFactory;

    private final LocalTimeResponseFactory localTimeFactory;

    @Override
    public CountryResponse convert(CountryData source) {
        List<LanguageResponse> languages =
                source.getLanguages().stream()
                        .map(languageConverter::convert)
                        .collect(Collectors.toList());

        List<CurrencyResponse> currencies =
                source.getCurrencies().stream()
                        .map(CurrencyData::getCode)
                        .map(currencyFactory::create)
                        .collect(Collectors.toList());

        Instant now = Instant.now();
        List<LocalTimeResponse> localTimes =
                source.getTimezones().stream()
                        .map(ZoneId::of)
                        .map(zoneId -> localTimeFactory.create(now, zoneId))
                        .collect(Collectors.toList());

        Iterator<BigDecimal> iterator = source.getLocation().iterator();
        BigDecimal latitude = iterator.next();
        BigDecimal longitude = iterator.next();

        return CountryResponse.builder()
                .code(source.getAlpha2Code())
                .code3(source.getAlpha3Code())
                .name(source.getName())
                .nativeName(source.getNativeName())
                .languages(languages)
                .currencies(currencies)
                .location(
                        LocationResponse.builder().latitude(latitude).longitude(longitude).build())
                .localTimes(localTimes)
                .utcTime(localTimeFactory.create(now, ZoneId.of("UTC")))
                .build();
    }
}
