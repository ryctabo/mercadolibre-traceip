/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.service;

import com.gitlab.ryctabo.ml.traceip.client.api.CountryClient;
import com.gitlab.ryctabo.ml.traceip.client.api.IpLocationClient;
import com.gitlab.ryctabo.ml.traceip.client.domain.CountryData;
import com.gitlab.ryctabo.ml.traceip.client.domain.IpLocationData;
import com.gitlab.ryctabo.ml.traceip.client.domain.LocationData;
import com.gitlab.ryctabo.ml.traceip.domain.IpTraceData;
import com.gitlab.ryctabo.ml.traceip.domain.config.DistanceStatistic;
import com.gitlab.ryctabo.ml.traceip.repository.DistanceStatisticRepository;
import com.gitlab.ryctabo.ml.traceip.util.EarthLocationUtils;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@Service
@RequiredArgsConstructor
public class IpTracerService {

    private final IpLocationClient ipLocationClient;

    private final CountryClient countryClient;

    private final LocationData baseLocation;

    private final DistanceStatisticRepository distanceStatisticRepository;

    public Optional<IpTraceData> trace(String ipAddress) {
        return ipLocationClient
                .getByIp(ipAddress)
                .map(IpLocationData::getCountryCode3)
                .flatMap(countryClient::getByCode)
                .map(countryData -> this.createIpTraceData(ipAddress, countryData));
    }

    private IpTraceData createIpTraceData(String ipAddress, CountryData countryData) {
        Iterator<BigDecimal> iterator = countryData.getLocation().iterator();
        BigDecimal latitude = iterator.next();
        BigDecimal longitude = iterator.next();
        LocationData location = new LocationData(latitude, longitude);

        BigDecimal distance = EarthLocationUtils.calculateDistance(location, baseLocation);
        this.registerDistance(distance);

        return IpTraceData.builder()
                .ipAddress(ipAddress)
                .country(countryData)
                .baseLocation(baseLocation)
                .distance(distance)
                .timestamp(ZonedDateTime.now())
                .build();
    }

    private void registerDistance(BigDecimal distance) {
        DistanceStatistic statistic = distanceStatisticRepository.get();

        statistic.setMax(statistic.getMax().max(distance));
        statistic.setMin(statistic.getMin().min(distance));
        statistic.addDistance(distance);

        distanceStatisticRepository.save(statistic);
    }
}
