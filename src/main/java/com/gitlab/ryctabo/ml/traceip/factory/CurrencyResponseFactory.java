/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.factory;

import com.gitlab.ryctabo.ml.traceip.client.api.ExchangeClient;
import com.gitlab.ryctabo.ml.traceip.client.domain.ExchangeRateData;
import com.gitlab.ryctabo.ml.traceip.domain.dto.CurrencyResponse;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@Component
@RequiredArgsConstructor
public class CurrencyResponseFactory {

    private final ExchangeClient exchangeClient;

    private static final String EXCHANGE_FORMAT = "1.00 EUR = %s %s";

    private static final String DECIMAL_FORMAT = "#,###.00";

    private final DecimalFormat dFormat = new DecimalFormat(DECIMAL_FORMAT);

    public CurrencyResponse create(String currencyCode) {
        return exchangeClient
                .exchangeEurTo(currencyCode)
                .map(this::create)
                .orElse(CurrencyResponse.builder().code(currencyCode).build());
    }

    private CurrencyResponse create(ExchangeRateData exchangeData) {
        String currencyCode = exchangeData.getRates().keySet().iterator().next();
        BigDecimal value = exchangeData.getRates().get(currencyCode);
        return CurrencyResponse.builder()
                .code(currencyCode)
                .exchange(String.format(EXCHANGE_FORMAT, dFormat.format(value), currencyCode))
                .build();
    }
}
