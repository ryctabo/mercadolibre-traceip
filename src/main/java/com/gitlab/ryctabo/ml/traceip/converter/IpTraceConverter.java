/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.converter;

import com.gitlab.ryctabo.ml.traceip.domain.IpTraceData;
import com.gitlab.ryctabo.ml.traceip.domain.dto.IpTraceResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@Component
@RequiredArgsConstructor
public class IpTraceConverter implements Converter<IpTraceData, IpTraceResponse> {

    private final CountryConverter countryConverter;

    private final LocationConverter locationConverter;

    @Override
    public IpTraceResponse convert(IpTraceData source) {
        return IpTraceResponse.builder()
                .ipAddress(source.getIpAddress())
                .country(countryConverter.convert(source.getCountry()))
                .baseLocation(locationConverter.convert(source.getBaseLocation()))
                .distance(source.getDistance())
                .timestamp(source.getTimestamp())
                .build();
    }
}
