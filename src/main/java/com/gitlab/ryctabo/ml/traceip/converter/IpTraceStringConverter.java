/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.converter;

import com.gitlab.ryctabo.ml.traceip.domain.dto.*;
import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@Component
public class IpTraceStringConverter implements Converter<IpTraceResponse, String> {

    private static final String FORMAT =
            "IP: %s, date: %s\n"
                    + "\tCountry: %s (%s)\n"
                    + "\tISO Code: %s\n"
                    + "\tLanguages: %s\n"
                    + "\tCurrencies: %s\n"
                    + "\tLocal time: %s\n"
                    + "\tDistance: %s km (%s to %s)\n";

    private static final String ITEM_FORMAT = "%s (%s)";

    private static final String DATETIME_PATTERN = "yyyy/MM/dd HH:mm:ss";

    private static final String DECIMAL_FORMAT = "#,###.00";

    @Override
    public String convert(IpTraceResponse source) {
        final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATETIME_PATTERN);
        final DecimalFormat decimalFormatter = new DecimalFormat(DECIMAL_FORMAT);

        return String.format(
                FORMAT,
                source.getIpAddress(),
                dateFormatter.format(source.getTimestamp()),
                source.getCountry().getName(),
                source.getCountry().getNativeName(),
                source.getCountry().getCode(),
                source.getCountry().getLanguages().stream()
                        .map(this::formatLanguage)
                        .collect(Collectors.joining(",")),
                source.getCountry().getCurrencies().stream()
                        .map(this::formatCurrency)
                        .collect(Collectors.joining(",")),
                Stream.concat(
                                source.getCountry().getLocalTimes().stream(),
                                Stream.of(source.getCountry().getUtcTime()))
                        .distinct()
                        .map(LocalTimeResponse::getFormatted)
                        .collect(Collectors.joining(",")),
                decimalFormatter.format(source.getDistance()),
                formatLocation(source.getBaseLocation()),
                formatLocation(source.getCountry().getLocation()));
    }

    private String formatCurrency(CurrencyResponse currency) {
        return String.format(ITEM_FORMAT, currency.getCode(), currency.getExchange());
    }

    private String formatLanguage(LanguageResponse language) {
        return String.format(ITEM_FORMAT, language.getName(), language.getCode());
    }

    private String formatLocation(LocationResponse location) {
        return String.format("%s, %s", location.getLatitude(), location.getLongitude());
    }
}
