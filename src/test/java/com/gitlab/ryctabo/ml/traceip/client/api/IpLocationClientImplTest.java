/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.client.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.ryctabo.ml.traceip.client.domain.IpLocationData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.web.client.RestTemplate;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@SpringBootTest
class IpLocationClientImplTest {

    @Mock private RestTemplate restTemplate;

    @InjectMocks private IpLocationClientImpl ipLocationClient;

    @Value("classpath:mocks/ip2country-response.json")
    private Resource ipLocationResponse;

    private static final String DEFAULT_IP = "1.2.3.4";

    @BeforeEach
    void setUp() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        IpLocationData ipLocationData =
                objectMapper.readValue(
                        ipLocationResponse.getInputStream().readAllBytes(), IpLocationData.class);

        when(restTemplate.getForObject(anyString(), eq(IpLocationData.class), eq("1.2.3.4")))
                .thenReturn(ipLocationData);
    }

    @Test
    void testGetByIdSuccessful() {
        assertThat(ipLocationClient.getByIp(DEFAULT_IP))
                .isNotEmpty()
                .map(IpLocationData::getCountryCode3)
                .hasValue("COL");
    }

    @Test
    void testGetByIdFailed() {
        assertThat(ipLocationClient.getByIp("INVALID_IP")).isEmpty();
    }
}
