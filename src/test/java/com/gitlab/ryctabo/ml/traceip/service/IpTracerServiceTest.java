/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gitlab.ryctabo.ml.traceip.client.api.CountryClient;
import com.gitlab.ryctabo.ml.traceip.client.api.IpLocationClient;
import com.gitlab.ryctabo.ml.traceip.client.domain.CountryData;
import com.gitlab.ryctabo.ml.traceip.client.domain.IpLocationData;
import com.gitlab.ryctabo.ml.traceip.client.domain.LocationData;
import com.gitlab.ryctabo.ml.traceip.domain.IpTraceData;
import com.gitlab.ryctabo.ml.traceip.domain.config.DistanceStatistic;
import com.gitlab.ryctabo.ml.traceip.repository.DistanceStatisticRepositoryImpl;
import java.math.BigDecimal;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@SpringBootTest
class IpTracerServiceTest {

    @Mock private IpLocationClient ipLocationClient;

    @Value("classpath:mocks/ip2country-response.json")
    private Resource ipLocationResponse;

    @Mock private CountryClient countryClient;

    @Value("classpath:mocks/restcountries-response.json")
    private Resource countyResponse;

    @Mock private DistanceStatisticRepositoryImpl distanceStatisticRepository;

    private IpTracerService ipTracerService;

    @BeforeEach
    void setUp() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());

        // Mocking the ip location client
        IpLocationData ipLocationData =
                objectMapper.readValue(
                        ipLocationResponse.getInputStream().readAllBytes(), IpLocationData.class);
        when(ipLocationClient.getByIp(anyString())).thenReturn(Optional.of(ipLocationData));

        // Mocking the country client call
        CountryData countryData =
                objectMapper.readValue(
                        countyResponse.getInputStream().readAllBytes(), CountryData.class);
        when(countryClient.getByCode(anyString())).thenReturn(Optional.of(countryData));

        // Mocking the distance statistic repository
        when(distanceStatisticRepository.get()).thenReturn(new DistanceStatistic());
        doNothing().when(distanceStatisticRepository).save(any());

        LocationData location = new LocationData(BigDecimal.ZERO, BigDecimal.ZERO);
        ipTracerService =
                new IpTracerService(
                        ipLocationClient, countryClient, location, distanceStatisticRepository);
    }

    @Test
    void testTraceByIpAddress() {
        assertThat(ipTracerService.trace("1.2.3.4"))
                .isNotEmpty()
                .map(IpTraceData::getCountry)
                .map(CountryData::getAlpha3Code)
                .hasValue("COL");

        verify(distanceStatisticRepository, times(1)).save(any());
        verify(countryClient, times(1)).getByCode("COL");
        verify(ipLocationClient, times(1)).getByIp("1.2.3.4");
    }
}
