/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.controller;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gitlab.ryctabo.ml.traceip.client.api.ExchangeClient;
import com.gitlab.ryctabo.ml.traceip.client.domain.ExchangeRateData;
import com.gitlab.ryctabo.ml.traceip.domain.IpTraceData;
import com.gitlab.ryctabo.ml.traceip.service.IpTracerService;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@SpringBootTest
@AutoConfigureMockMvc
class TraceControllerTest {

    @Autowired private MockMvc mvc;

    @MockBean private IpTracerService ipTracerService;

    @MockBean private ExchangeClient exchangeClient;

    @Value("classpath:mocks/ip-trace-response.json")
    private Resource ipTraceResponse;

    @Value("classpath:mocks/fixer-io-response.json")
    private Resource exchangeResponse;

    private static final String MOCK_IP_ADDRESS = "5.6.7.8";

    @BeforeEach
    void setUp() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());

        IpTraceData ipTraceData =
                objectMapper.readValue(
                        ipTraceResponse.getInputStream().readAllBytes(), IpTraceData.class);

        ExchangeRateData exchangeRateData =
                objectMapper.readValue(
                        exchangeResponse.getInputStream().readAllBytes(), ExchangeRateData.class);

        when(ipTracerService.trace(anyString())).thenReturn(Optional.of(ipTraceData));
        when(exchangeClient.exchangeEurTo(anyString())).thenReturn(Optional.of(exchangeRateData));
    }

    @Test
    void testResponseAsApplicationJson() throws Exception {
        mvc.perform(get("/trace/{ipAddress}", MOCK_IP_ADDRESS).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.ipAddress", is(MOCK_IP_ADDRESS)))
                .andExpect(jsonPath("$.country", notNullValue()))
                .andExpect(jsonPath("$.country.name", is("Spain")))
                .andExpect(jsonPath("$.country.code", is("ES")))
                .andExpect(jsonPath("$.country.code3", is("ESP")))
                .andExpect(jsonPath("$.baseLocation", notNullValue()))
                .andExpect(jsonPath("$.distance", isA(Double.class)))
                .andExpect(jsonPath("$.timestamp", isA(String.class)))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    void testResponseAsTextPlain() throws Exception {
        mvc.perform(get("/trace/{ipAddress}", MOCK_IP_ADDRESS).accept(MediaType.TEXT_PLAIN))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(MOCK_IP_ADDRESS)))
                .andExpect(content().string(containsString("Country: Spain")))
                .andExpect(content().string(containsString("ISO Code: ES")))
                .andExpect(content().string(containsString("Languages: Spanish (es)")))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN));
    }

    @Test
    void respondBadRequestByInvalidIp() throws Exception {
        mvc.perform(get("/trace/{ipAddress}", "INVALID_IP").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("The IP address is invalid")));
    }
}
