/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.converter;

import static org.assertj.core.api.Assertions.assertThat;

import com.gitlab.ryctabo.ml.traceip.domain.dto.*;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@SpringBootTest
class IpTraceStringConverterTest {

    @Autowired private IpTraceStringConverter ipTraceStringConverter;

    @Test
    void testConverter() {
        LanguageResponse language = LanguageResponse.builder().code("es").name("Spanish").build();

        CurrencyResponse currency =
                CurrencyResponse.builder().code("COP").exchange("CURRENCY_FORMATTED").build();

        LocalTimeResponse timezone1 =
                LocalTimeResponse.builder()
                        .timezone(ZoneId.of("UTC-5"))
                        .time(LocalTime.now())
                        .formatted("DATE_FORMATTED1")
                        .build();

        LocalTimeResponse timezone2 =
                LocalTimeResponse.builder()
                        .timezone(ZoneId.of("UTC"))
                        .time(LocalTime.now())
                        .formatted("DATE_FORMATTED2")
                        .build();

        LocationResponse location =
                LocationResponse.builder()
                        .latitude(BigDecimal.valueOf(-40d))
                        .longitude(BigDecimal.valueOf(-36d))
                        .build();

        CountryResponse country =
                CountryResponse.builder()
                        .code("CO")
                        .code3("COL")
                        .name("Colombia")
                        .nativeName("Colombia")
                        .languages(List.of(language))
                        .currencies(List.of(currency))
                        .localTimes(List.of(timezone1))
                        .utcTime(timezone2)
                        .location(location)
                        .build();

        IpTraceResponse ipTraceResponse =
                IpTraceResponse.builder()
                        .ipAddress("1.2.3.4")
                        .country(country)
                        .baseLocation(location)
                        .distance(BigDecimal.valueOf(12500.02d))
                        .timestamp(ZonedDateTime.now())
                        .build();

        assertThat(ipTraceStringConverter.convert(ipTraceResponse))
                .isNotNull()
                .contains(
                        "1.2.3.4",
                        "Colombia (Colombia)",
                        "ISO Code: CO",
                        "Languages: Spanish (es)");
    }
}
