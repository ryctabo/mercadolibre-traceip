/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.client.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gitlab.ryctabo.ml.traceip.client.domain.ExchangeRateData;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.web.client.RestTemplate;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@SpringBootTest
class ExchangeClientImplTest {

    @Mock private RestTemplate restTemplate;

    private String accessKey = "DEFAULT_ACCESS_KEY";

    private ExchangeClientImpl exchangeClient;

    @Value("classpath:mocks/fixer-io-response.json")
    private Resource exchangeResponse;

    @BeforeEach
    void setUp() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        ExchangeRateData exchangeRateData =
                objectMapper.readValue(
                        exchangeResponse.getInputStream().readAllBytes(), ExchangeRateData.class);

        when(restTemplate.getForObject(
                        anyString(), eq(ExchangeRateData.class), eq(accessKey), eq("COP")))
                .thenReturn(exchangeRateData);

        exchangeClient = new ExchangeClientImpl(restTemplate, accessKey);
    }

    @Test
    void testExchangeEurToCopSuccessful() {
        assertThat(exchangeClient.exchangeEurTo("COP"))
                .isNotEmpty()
                .map(ExchangeRateData::getRates)
                .map(Map::keySet)
                .map(Set::iterator)
                .map(Iterator::next)
                .hasValue("COP");
    }

    @Test
    void testExchangeEurToBadCurrency() {
        assertThat(exchangeClient.exchangeEurTo("BAD_CURRENCY_CODE")).isEmpty();
    }
}
