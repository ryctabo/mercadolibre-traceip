/*
 * Copyright (c) 2021 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.ryctabo.ml.traceip.util;

import static org.assertj.core.api.Assertions.assertThat;

import com.gitlab.ryctabo.ml.traceip.client.domain.LocationData;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
class EarthLocationUtilsTest {

    @Test
    void testCalculateDistance() {
        LocationData p1 = new LocationData(BigDecimal.valueOf(-34), BigDecimal.valueOf(-64));
        LocationData p2 = new LocationData(BigDecimal.valueOf(40), BigDecimal.valueOf(-4));

        assertThat(EarthLocationUtils.calculateDistance(p1, p2))
                .isNotNull()
                .isPositive()
                .isEqualTo(BigDecimal.valueOf(10274.59));
    }
}
