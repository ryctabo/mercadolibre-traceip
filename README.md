# MercadoLibre TraceIP

Technical challenge for MercadoLibre https://www.mercadolibre.com.co/

Create an application that from IP address finds the country where 
it belongs, and show the following information:

- Name and ISO code of the country
- Official languages
- Local date and times
- Distance estimate from the country to Buenos Aires, Argentina.
- Local currency and its conversions to EUR or USD

E.g:

```
> traceip 1.2.3.4

IP: 1.2.3.4, date: 2021/07/28 16:23:58
        Country: United States of America (United States)
        ISO Code: US
        Languages: English (en)
        Currencies: USD (1 USD = 1 USD)
        Local time: 20:01:23 (UTC), 21:01:23 (UTC+01:00)
        Distance: 10.354 km (-34,-64 to 38,-97)
```

## Getting Started

You need to have installed [JDK 11](http://openjdk.java.net/) or 
greater on you local machine.

## Running with Docker

First, you need to install [docker](https://www.docker.com/) and install
`docker-compose`.
Run the following command:

```bash
docker-compose up -d
```

You can use the bash script called `traceip` with the following 
command: `./traceip 1.2.3.4` or you can use `curl` command:

```bash
curl -X GET localhost:8080/trace/1.2.3.4 -H "Accept: text/plain"
```

Replace `1.2.3.4` with you IP address.

## Development Time

The project use [Gradle](https://gradle.org/) as a manager and to 
automate tasks. Use `build` gradle task to download local dependencies
and compile the project and use `clean` to clean it.

Clean and build the project:

```bash
./gradlew clean build
```

### Running Project

If you need to run the project you need to use the following command:

```bash
./gradlew bootRun
```

### Running Tests

You need to use `test` task of gradle:

```bash
./gradlew test
```

## Deployment

Just build the project you can get the packaged file `*.jar` in the folder
`/build/libs`. with the name of the project and the corresponding version.

If in some case you are not able to run the following command to generate
the file:

```bash
./gradlew assemble
```

All development rights are reserved by **Gustavo Pacheco**.
